# dotfiles

My configuration files, used on various Linux Systems (CentOS, Debian, Ubuntu,
Minit, ...), Cygwin and WSL.

# Version

Release: 0.4.1
