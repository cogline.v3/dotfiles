# .bash_profile

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

# languæge detection
unset LANG

if locale -a|grep -q 'de_DE.utf8'; then
  export LANG=de_DE.UTF-8
elif locale -a|grep -q 'en_US.utf8'; then
  export LANG=en_US.UTF-8
elif locale -a|grep -q 'C.UTF-8'; then
  export LANG=C.UTF-8
else
  export LANG=C
fi

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi
