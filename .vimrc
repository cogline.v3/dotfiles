"=============================================================================
" 
" Vim configuration file
"
"=============================================================================

"-----------------------------------------------------------------------------
" Hint: Unix and Windows
"
" For plugins and other personal settings, the directory "~/.vim" is used
" under Unix and the directory "$HOME/vimfiles" is used under Windows.
"
" To ensure that the configuration file can be used on both systems, it is
" recommended to create a directory "$HOME/.vim" under Windows and then use
" the command mklink to create a symbolic link from "$HOME/vimfiles" to
" "$HOME/.vim":
"
" cmd /c mklink /J vimfiles .vim
"-----------------------------------------------------------------------------

"-----------------------------------------------------------------------------
" Ensure to set windows environments variable $HOME for the current user to
" the $USERPROFILE
"-----------------------------------------------------------------------------
if has("win32")
  let $HOME = $USERPROFILE
  source $VIMRUNTIME/mswin.vim
  unmap <C-F>
  unmap <C-V>
endif
set nocompatible

"-----------------------------------------------------------------------------
" Set directories for backup, swap and undo files
"-----------------------------------------------------------------------------
if !isdirectory(expand("~/.vim/backup"))
  call mkdir(expand("~/.vim/backup"), "p")
endif
set backupdir=~/.vim/backup//

if !isdirectory(expand("~/.vim/swap"))
  call mkdir(expand("~/.vim/swap"), "p")
endif
set directory=~/.vim/swap//

if !isdirectory(expand("~/.vim/undo"))
  call mkdir(expand("~/.vim/undo"), "p")
endif
set undodir=~/.vim/undo//

"-----------------------------------------------------------------------------
" Set proxy server if necessary
"-----------------------------------------------------------------------------
" let $http_proxy = 'http://username:password@proxy.server:port'
" let $https_proxy = 'http://username:password@proxy.server:port'

"-----------------------------------------------------------------------------
" Installation of vim-plug, a minimalist Vim plugin manager.
"-----------------------------------------------------------------------------
"
" Download plug.vim and put it in the "autoload" directory.
"
" Unix:
"   curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"     https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

" Windows (PowerShell):
"   iwr -useb https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim |`
"       ni $HOME/vimfiles/autoload/plug.vim -Force
"
" Windows (With proxy):
"   iwr -Proxy http://username:password@proxy.server:port |`
"       -useb https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim |`
"       ni $HOME/vimfiles/autoload/plug.vim -Force
"
" Usage:
" :PlugInstall to install the plugins
" :PlugUpdate to install or update the plugins
" :PlugDiff to review the changes from the last update
" :PlugClean to remove plugins no longer in the list
" 
"-----------------------------------------------------------------------------
call plug#begin('~/.vim/plugged')
" file system explorer for the Vim editor
Plug 'preservim/nerdtree'
" solarized colorscheme for Vim
Plug 'altercation/vim-colors-solarized'
" syntax coloring and indenting for Windows PowerShell (.ps1) files
Plug 'pprovost/vim-ps1'
" vim syntax plugin for Ansible 2.x
Plug 'pearofducks/ansible-vim'
" A plugin for editing xml
Plug 'othree/xml.vim'
" UltiSnips is the ultimate solution for snippets
" This plugin needs Python3
" Windows (PowerShell):
"   Run the command "winget search Python.Python" to see the available Python
"   versions. Once you find the desired version, use the command "winget
"   install" followed by the version's ID to install it. For example, for
"   version 3.12, run "winget install Python.Python.3.12".
Plug 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'
call plug#end()

"-----------------------------------------------------------------------------
" General settings
"-----------------------------------------------------------------------------
" Set UTF-8 as default encoding
set encoding=utf-8
set fileencoding=utf-8

set fileformat=unix             " Set Unix as default file format

set backup                      " Keep a backup file
set number                      " Show line numbers
set cursorline                  " Highlight the current line
set expandtab                   " Convert tabs to spaces
set shiftwidth=2                " Width for automatic indentation
set softtabstop=2               " Insert number of spaces instead of tabs
set tabstop=2                   " Tab width
"set clipboard=unnamedplus      " Shared clipboard with system

set backspace=indent,eol,start	" More powerful backspacing
set visualbell                  " Errorbells: damn this beep!  ;-)
set autoindent                  " Always set autoindenting on
set wrap                        " lines will wrap
set linebreak                   " Don't wrap words by default
set textwidth=78                " Controls the wrap width
set viminfo='20,\"100           " read/write a .viminfo file, don't store more
                                " than 100 lines of registers
set history=200                 " keep 200 lines of command line history
set ruler                       " show the cursor position all the time

set showcmd                     " Show (partial) command in status line.
set showmatch                   " Show matching brackets.
set showmode                    " If in Insert, Replace or Visual mode put a
                                " message on the last line.
set ignorecase                  " Do case insensitive matching
set incsearch                   " While typing a search pattern, show
                                " immediately where the so far typed pattern
                                " matches. see also "help incsearch"
set hlsearch                    " Enables highlighting of all matches of a
                                " search term in the entire text.

"-----------------------------------------------------------------------------
" Enabled file type detection
"-----------------------------------------------------------------------------
if has("autocmd")
  " Use the default filetype settings. If you also want to load indent files
  " to automatically do language-dependent indenting add 'indent' as well.
  filetype plugin indent on
endif " has ("autocmd")

"-----------------------------------------------------------------------------
" Check the existence of plugins
"-----------------------------------------------------------------------------
" Check for NERDTree
if filereadable(expand("~/.vim/plugged/nerdtree/plugin/NERD_tree.vim"))
  let nerdtree=1 
else
  let nerdtree=0 
endif

" Check for german spell file
"  Download spell files first (if necessary):
"   Unix (Bash):
"     curl "https://ftp.nluug.nl/pub/vim/runtime/spell/de.utf-8.spl" -L \
"           --create-dirs -o ~/.vim/spell/de.utf-8.spl
"     curl "https://ftp.nluug.nl/pub/vim/runtime/spell/de.utf-8.sug" -L \
"           --create-dirs -o ~/.vim/spell/de.utf-8.sug
"   Windows (PowerShell):
"     iwr -useb https://ftp.nluug.nl/pub/vim/runtime/spell/de.utf-8.spl |`
"           ni $HOME/vimfiles/spell/de.utf-8.spl -Force
"     iwr -useb https://ftp.nluug.nl/pub/vim/runtime/spell/de.utf-8.sug |`
"           ni $HOME/vimfiles/spell/de.utf-8.sug -Force
"   Windows (with proxy):
"     iwr -Proxy http://username:password@proxy.server:port |`
"         -useb https://ftp.nluug.nl/pub/vim/runtime/spell/de.utf-8.spl |`
"         ni $HOME/vimfiles/spell/de.utf-8.spl -Force
"     iwr -Proxy http://username:password@proxy.server:port |`
"         -useb https://ftp.nluug.nl/pub/vim/runtime/spell/de.utf-8.sug |`
"         ni $HOME/vimfiles/spell/de.utf-8.sug -Force
if filereadable(expand("~/.vim/spell/de.utf-8.spl"))
  let german_spell_check=1 
else
  let german_spell_check=0 
endif

" Check for Ansible syntax plugin
if filereadable(expand("~/.vim/plugged/ansible-vim/ftplugin/ansible.vim"))
  let ansible=1 
else
  let ansible=0 
endif

" Check for UltiSnips snippet plugin
if filereadable(expand("~/.vim/plugged/ultisnips/plugin/UltiSnips.vim"))
  let ultisnips=1 
else
  let ultisnips=0 
endif

" Check for Solarized color scheme
if filereadable(expand("~/.vim/plugged/vim-colors-solarized/colors/solarized.vim"))
  let solarized_colors=1 
else
  let solarized_colors=0 
endif

"-----------------------------------------------------------------------------
" Specific settings for XML files
"-----------------------------------------------------------------------------
" In order for tag matching to work with '%' in XML files, a directory
" "~/.vim/plugin" must exist into which the file "matchit.vim" is copied or
" linked. The file is normaly shipped with "Vim".
"
" For example:
"   ln -s /usr/share/vim/vim91/macros/matchit.vim ~/.vim/plugin/matchit.vim
augroup XML
  autocmd!
  " Set tabstop to 2
  autocmd FileType xml,xsd,xhtml,html,docbk setlocal tabstop=2 shiftwidth=2 expandtab
  " Enable syntax foldmethod
  autocmd FileType xml,xsd,xhtml,html,docbk let g:xml_syntax_folding=1
  " Set foldmethod
  autocmd FileType xml,xsd,xhtml,html,docbk setlocal foldmethod=syntax
  " Open all folds when loading the file
  autocmd FileType xml,xsd,xhtml,html,docbk :syntax on
  autocmd FileType xml,xsd,xhtml,html,docbk normal zR
augroup END

"-----------------------------------------------------------------------------
" Specific settings for YAML and Ansible files
"-----------------------------------------------------------------------------
augroup YML
  autocmd!
  " Set tabstop to 2
  autocmd FileType yaml,yaml.ansible setlocal tabstop=2 shiftwidth=2 expandtab
  " Set foldmethod 
  autocmd FileType yaml,yaml.ansible setlocal foldmethod=indent
  " Open all folds when loading the file
  autocmd FileType yaml,yaml.ansible normal zR
augroup END

"-----------------------------------------------------------------------------
" Color settings
"-----------------------------------------------------------------------------
syntax enable                   " Enable syntax highlighting

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
set background=dark
set t_Co=256                    " Use 256 colors in terminal

if solarized_colors
  if has("gui_running")
    let g:solarized_termcolors = 256
  else
    let g:solarized_termtrans = 1
  endif
  silent! colorscheme solarized
endif

"-----------------------------------------------------------------------------
" Status line
"-----------------------------------------------------------------------------
set laststatus=2                " Always show a status line
if has("statusline")
 set statusline=%<%f\ %h%m%r%=%{\"[\".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\",B\":\"\").\"]\ \"}%k\ %-14.(%l,%c%V%)\ %P
endif

"-----------------------------------------------------------------------------
" Spell check
"-----------------------------------------------------------------------------
if german_spell_check
  " Set keyboard macro
  map <F8>  :setlocal spell spelllang=de <return>
endif

"-----------------------------------------------------------------------------
" NERDTree configuration
"-----------------------------------------------------------------------------
if nerdtree
  " map specific keys or shortcuts to open NERDTree
  nnoremap <leader>n :NERDTreeFocus<CR>
  nnoremap <C-n> :NERDTree<CR>
  nnoremap <C-t> :NERDTreeToggle<CR>
  " nnoremap <C-f> :NERDTreeFind<CR>

  " Show hidden files in NERDTree
  " let g:NERDTreeShowHidden=1
  
  " Start NERDTree when Vim is started without file arguments.
  " autocmd StdinReadPre * let s:std_in=1
  " autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif

  " Exit Vim if NERDTree is the only window remaining in the only tab.
  autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | call feedkeys(":quit\<CR>:\<BS>") | endif

  " Close the tab if NERDTree is the only window remaining in it.
  autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | call feedkeys(":quit\<CR>:\<BS>") | endif

  " Open the existing NERDTree on each new tab.
  autocmd BufWinEnter * if &buftype != 'quickfix' && getcmdwintype() == '' | silent NERDTreeMirror | endif
endif

"-----------------------------------------------------------------------------
" Ansible syntax
"-----------------------------------------------------------------------------
if ansible
  au BufRead,BufNewFile */playbooks/*.yml set filetype=yaml.ansible
  let g:ansible_name_highlight = 'd'
  let g:ansible_extra_keywords_highlight = 1
endif


"-----------------------------------------------------------------------------
" UltiSnips snippet plugin
"-----------------------------------------------------------------------------
if ultisnips
  let g:UltiSnipsExpandTrigger="<tab>"
  let g:UltiSnipsJumpForwardTrigger="<c-b>"
  let g:UltiSnipsJumpBackwardTrigger="<c-z>"

  " If you want :UltiSnipsEdit to split your window.
  let g:UltiSnipsEditSplit="vertical"
endif

"-----------------------------------------------------------------------------
" GUI configuration
"-----------------------------------------------------------------------------
if has("gui_running")
  if has("win32")
    set guifont=Consolas:h12:cANSI
  else
	set guifont=Ubuntu\ Mono\ Medium\ 16
  endif
  set co=120                    " number of columns
  set lines=57                  " number of lines
  if nerdtree
	" Start NERDTree, unless a file or session is specified, eg. vim -S session_file.vim.
  	autocmd StdinReadPre * let s:std_in=1
  	autocmd VimEnter * if argc() == 0 && !exists('s:std_in') && v:this_session == '' | silent NERDTree | wincmd p | endif
  endif
endif

"-----------------------------------------------------------------------------
" Start new session at line last edited position.
" Remembers where you left off. -> cool feature!
"-----------------------------------------------------------------------------
autocmd BufReadPost * if line("'\"") && line("'\"") <= line("$") | exe "normal `\"" | endif
