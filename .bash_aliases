# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -lF'
alias la='ls -alF'
alias l='ls -CF'

# Ansible settings
alias ap='ansible-playbook'
export ANSIBLE_FORCE_COLOR=1

# Vim settings
alias vi='vim'
export EDITOR=vim

# sudo settinigs
# If the last character of the alias value is a blank, then the next command wordcw
# following the alias is also checked for alias expansion.
alias sudo='sudo '

# Docker settings
alias dc='docker-compose'
alias di='docker images --format "table {{.Repository}}\t{{.Tag}}\t{{.CreatedSince}}"'

